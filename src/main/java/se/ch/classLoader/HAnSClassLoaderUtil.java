package se.ch.classLoader;

import com.atlassian.plugin.classloader.PluginsClassLoader;
import com.intellij.ide.plugins.IdeaPluginDescriptor;
import com.intellij.ide.plugins.PluginManagerCore;
import com.intellij.openapi.extensions.PluginId;
import se.ch.hans.featureModel.psi.FeatureModelVisitor;
import com.atlassian.plugin.classloader.PluginClassLoader;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;


public class HAnSClassLoaderUtil   {

    final static String HAnS_WP_PLUGIN_DIR = "/Users/mellome1992/LocalRepository-Public/Plugins/[ver.20]HAnS-1.0.zip";
    final static String LOAD_CLASS_NAME = "se.ch.hans.toolwindow.HansViewerWindowService";

    public static void UsingAtlassianPluginAPI() throws ClassNotFoundException {
        File pluginFile = new File(HAnS_WP_PLUGIN_DIR);
        PluginClassLoader pcl = new PluginClassLoader(pluginFile);
        Class<?> clazz = pcl.loadClass(LOAD_CLASS_NAME);
        System.out.println(clazz.getName());
    }

    public static void UsingPluginManager(){
        PluginId pluginId = PluginId.getId("java.io.IOException");
        IdeaPluginDescriptor plugin = PluginManagerCore.getPlugin(pluginId);

        if (plugin == null) {
            System.out.println("NO PLUGIN BEEN FOUND");
        }
        Path path = plugin.getPluginPath();
        String pluginPath= path.toFile().getPath();
        System.out.println(pluginPath);
    }

    public static void UsingClassLoader(){
        // try to find out the problem 'ClassNotFoundException' came from.
        try {
            System.out.println(Class.forName(LOAD_CLASS_NAME));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        ClassLoader classLoader = ClassLoader.getPlatformClassLoader();
        while (classLoader!=null){
            System.out.println(classLoader);
            classLoader = classLoader.getParent();
        }
        System.out.println(HAnSClassLoaderUtil.class.getClassLoader());
    }

    public static void UsingClassForName() throws ClassNotFoundException, MalformedURLException {
        ClassLoader loader = new URLClassLoader(new URL[]{
                new URL("file://"+HAnS_WP_PLUGIN_DIR)});
        Class<?> fmvC = loader.loadClass(LOAD_CLASS_NAME);
//        Class<?> fmvC = Class.forName("se.ch.hans.codeAnnotation.psi.CodeAnnotationElementType");
        try {
            FeatureModelVisitor fmvO = (FeatureModelVisitor) fmvC.getDeclaredConstructor().newInstance();
            System.out.println(fmvO.toString());
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws MalformedURLException, ClassNotFoundException {
        UsingAtlassianPluginAPI();
        //        UsingClassLoader();
//        UsingClassForName();
//        UsingPluginManager();
    }
}
